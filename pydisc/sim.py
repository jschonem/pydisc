# -*- coding: utf-8 -*-

import numpy as np
import transform
import scipy.integrate
import disc
import matplotlib.pyplot as plt

# TODO: Add integration of the yaw-free Euler angles
# TODO: Remove gamma as a state variable
# TODO: Write up methods and verification

class sim:
    """
    Manages simulation process and results for a given disc object.
    """

    def __init__(self, disc, rho = 1.225, g = 9.81):
        """
        Initialize object with an existing disc object for simulation.
        """
        self.disc = disc
        self.x0 = np.zeros((3, ))
        self.v0 = np.zeros((3, ))
        self.omega0 = np.zeros((3, ))
        self.q0 = np.zeros((3, ))
        self.q40 = np.ones((1, ))
        self.euler0 = np.zeros((3, ))
        self.windvec = np.zeros((3, ))

        self.rho = rho
        self.g = g
        self.gvec = np.array([0, 0, self.g])

        # Solution variables
        self.t = None
        self.x = None
        self.v = None
        self.omega = None
        self.q = None
        self.q4 = None
        self.euler = None
        
        # Flag for Euler angle integration
        self.integrateEuler = False

    def eom(self, t, x):
        """
        Equation of motion for time integration. State variables are stored in
        the order: x = array([X, Vb, omega, q, q4, euler]), with:

            X: Position of the disc center [length-3 array]
            V: Velocity of the disc center [length-3 array]
            omega: Angular velocity of the disc CG, no-spin [length-3 array]
            q, q4: Quaternions [length-3 array], [scalar]
            euler: 3-2-1 Intrinsic Euler angles as given by Etkin Section 4.4

        The EOMs are integrated in the body-fixed frame, except for the
        position which is in the Earth frame. The quaternions are used to 
        describe the orientation of the body frame; Euler angles are for
        reference only, and are only integrated if the sim.integrateEuler flag
        is set to True.
        """

        # Assign quantities of convenience
        Vb = x[3:6]  # Body-fixed groundspeed (not airspeed)
        omegab = x[6:9]
        q = x[9:12]
        q4 = x[12]
        euler = x[13:16]

        # Transform velocity to body fixed frame; include wind effects
#        rbe = transform.quat2r(q, q4)  # Transpose?
        rbe = transform.euler2r(*euler)
        V = Vb - rbe @ self.windvec  # Airpseed, including wind (Etkin 1.6,1)

        # Obtain the aerodynamic forces and moments (Section 7.1 of Crowther)
        Faerob, Maerob = self.disc.getAeroForces(V, omegab, self.rho)

        # Obtain the current angular momentum of the disc (Etkin Eq. 4.6,1)
        hBody = self.disc.getAngularMomentum(omegab)

        # Acceleration (Etkin Eq. 4.5,4)
        omegaHat = transform.skew(omegab)
        Ab = Faerob/self.disc.getMass() - omegaHat @ Vb + rbe @ self.gvec

        # Angular acceleration (Etkin Eq. 4.5,5)
        # Note there is a typo in Eq. 7.9 of Crowther & Potts
        omegad = self.disc.getInertiaInv() @ (Maerob - omegaHat @ hBody)

        # Obtain kinematic rotational rates of change (Wie Eq. 5.76)
        qd, q4d = transform.qdot(q, q4, omegab)
        
        # Integrate the Euler angle, per the integrateEuler flag. We define 
        # here a pseudo-angular velocity which does not include the yaw rate.
        if self.integrateEuler:
            eulerd = transform.omega2EulerRates(euler, omegab)
        else:
            eulerd = np.zeros((3, ))
        
        return np.hstack([rbe.T @ Vb, Ab, omegad, qd, q4d, eulerd])

    def getx0(self):
        """
        Obtain vector of initial conditions as currently set.
        """
        return np.hstack([self.x0, self.v0, self.omega0, self.q0, self.q40,
                          self.euler0])

    def terminateEvent(self, t, x):
        """
        Terminates the disc flight based on zero-height criteria.
        """

        return x[2]

    def integrate(self, T, nt = 100):
        """
        Integrates equations of motion with output given at supplied time
        vector. Assigns
        """

        x0 = self.getx0()
        t = np.linspace(0., T, nt)
        eventFun = lambda t, x: self.terminateEvent(t, x)
        eventFun.terminal = True

        sol = scipy.integrate.solve_ivp(self.eom, [0.0, T], x0,
                                        dense_output = True, t_eval = t,
                                        events = eventFun)

        # Unpack variables
        self.t = sol.t
        self.y = sol.y
        self.x = sol.y[:3, :].T
        self.v = sol.y[3:6, :].T
        self.omega = sol.y[6:9, :].T
        self.q = sol.y[9:12, :].T
        self.q4 = sol.y[12, :]
        self.euler = sol.y[13:16, :].T

        # Post-compute some other quanties and return
        nt = len(self.t)
        sideslip = np.zeros((nt, ))
        Vbody = np.zeros((3, nt))
        alpha = np.zeros((nt, ))
        lift = np.zeros((nt, ))
        drag = np.zeros((nt, ))
        moment = np.zeros((nt, ))

        for i in range(nt):
            q = self.q[i, :]
            q4 = self.q4[i]
            Vb = self.v[i, :]

            rbe = transform.quat2r(q, q4)  # Transpose?
            V = Vb - rbe @ self.windvec  # Airpseed, including wind (Etkin 1.6,1)
            Vbody[:, i] = V

            sideslip[i] = np.arctan2(V[1], V[0])  # Eq. 7.12 of Crowther
            # Body to zero-sideslip transformation

            vmag = np.linalg.norm(V)
            vhat = V/vmag
            alpha[i] = np.arcsin(vhat[-1])

            L, M = self.disc.getAeroForces(V, self.omega[i, :], self.rho)
            lift[i] = -L[2]
            drag[i] = -L[0]
            moment[i] = M[1]

        return Vbody, alpha, sideslip, lift, drag, moment

    def plotTrajectory(self, spacing = 1, view = 'top', **kwargs):
        """
        Plots the position of the disc as a function of time. "Spacing"
        provides the interval spacing used for plotting. "view" can be either
        'top', 'right', or a length-two vector giving the azimuth and bearing
        values for the desired view.
        """
        
        # Set default plot property values if not supplied
        if 'color' not in kwargs:
            kwargs['color'] = 'k'
        
        for i in range(0, len(self.t), spacing):
            self.disc.plot(self.y[:, i], showPosition = True, **kwargs)
            
        # Set view
        if type(view) is str:
            if view == 'top':  # Top-down
                plt.gca().view_init(-90, -90)
            if view == 'right':  # Side view
                plt.gca().view_init(0, -90)
        
        else:  # Arbitrary view
            plt.gca().view_init(view[0], view[1])
        
        # Set axis options
        plt.xlabel('X')
        plt.ylabel('Y')
        plt.gca().set_zlabel('Z')
        plt.axis('equal')
        plt.gca().invert_zaxis()

    def setX0(self, x0):
        """
        Sets initial position of disc.
        """
        # Input checks
        self.x0 = x0

    def setV0(self, v0):
        """
        Sets initial velocity of disc.
        """
        # Input checks
        self.v0 = v0

    def setOmega0(self, omega0):
        """
        Sets initial angular rate of disc.
        """
        self.omega0 = omega0

    def setq0(self, q0, q40):
        """
        Sets initial angular orientation of disc using quaternions.
        """
        self.q0 = q0
        self.q40 = q40

    def setWind(self, windvec):
        """
        Sets the wind vector for the system.
        """
        self.windvec = windvec

    def setAngleAxis(self, angle, axis):
        """
        Sets initial angular orientation of disc using Euler's angle/axis
        theorem.
        """
        raise NotImplementedError

    def setEulerAngle(self, phi, theta, psi):
        """
        Sets initial angular orientation of disc using Euler angles.
        """
        r = transform.euler2r(phi, theta, psi)
        q, q4 = transform.r2quat(r)
        self.setq0(q, q4)
        self.euler0 = np.array([phi, theta, psi])

    def setEulerRate(self, phi, theta, psi,
                           phid, thetad, psid):
        """
        Sets body-fixed angular velocities based on initial Euler rates; only
        usable for the 1-2-3 order.
        """
        self.setOmega0(transform.eulerRates2Omega(phi, theta, psi,
                                                  phid, thetad, psid))


#-----------------------------------------------------------------------------#
# Properties class
#-----------------------------------------------------------------------------#

class discprops:
    """
    Behaves as a structure to hold key mass/inertia/aerodynamic properties
    of a disc, with some additional useful helper functions.
    """

    def __init__(self, m, r, Iaxis, Ioff, Cl0, Clalpha, Cd0, Cdalpha, Cm0, 
                 Cmalpha, Cmq = -0.01, Crp = -0.01):

        self.m = m
        self.r = r
        self.Iaxis = Iaxis
        self.Ioff = Ioff
        self.Cl0 = Cl0
        self.Clalpha = Clalpha
        self.Cd0 = Cd0
        self.Cdalpha = Cdalpha
        self.Cm0 = Cm0
        self.Cmalpha = Cmalpha
        self.Cmq = Cmq
        self.Crp = Crp

    def getProps(self):

        return (self.m, self.r, self.Iaxis, self.Ioff, self.Cl0, self.Clalpha,
                self.Cd0, self.Cdalpha, self.Cm0, self.Cmalpha, 
                self.Cmq, self.Crp)

        
class initconds:
    """
    Behave as a structure to hold values of initial conditions, with some 
    additional useful helper functions.
    """
    
    def __init__(self, x0 = None, v0 = None, phi0 = 0.0, theta0 = 0.0, 
                 psi0 = 0.0, gamma = None, phid0 = 0.0, psid0 = 0.0, 
                 thetad0 = 0.0):
        """
        Create object with given values of initial conditions. Most defaults
        are zero except for the Z coordinate which defaults to 1 meter above
        ground. The supplied velocity v0 can be interpreted either as 
        body-fixed or Earth-fixed, depending on the flag used when ICs are
        assigned.
        """
        
        if x0 is None:
            x0 = np.zeros((3, ))
            x0[2] = -1.0
        if v0 is None:
            v0 = np.zeros((3, ))
            
        self.x0 = np.array(x0)
        self.v0 = np.array(v0)
        self.phi0 = phi0
        self.theta0 = theta0
        self.psi0 = psi0
        self.gamma = gamma
        self.phid0 = phid0
        self.thetad0 = 0.0
        self.psid0 = psid0
        
        
    def setIc(self, simobj, bodyFrameVelocity = True):
        """
        Assigns the initial conditions associated with this object to those 
        of "simobj," which must be an instance of the sim class. If the 
        bodyFrameVelocity flag is set to true, the initial velocity is 
        interpreted as body-fixed; otherwise it is considered Earth-fixed.
        """
        
        assert isinstance(simobj, sim)
        
        simobj.setX0(self.x0)
        simobj.setEulerAngle(self.phi0, self.theta0, self.psi0)
        
        if self.gamma is None:
            simobj.setEulerRate(self.phi0, self.theta0, self.psi0, 
                        self.phid0, self.thetad0, self.psid0) 
        else:
            simobj.setOmega0(np.array([0, 0, self.gamma]))
        
        if bodyFrameVelocity:
            # Align initial velocity with initial attitude
            simobj.setV0(self.v0)  
        else:
            R0 = transform.euler2r(self.phi0, self.theta0, self.psi0)
            simobj.setV0(R0 @ self.v0)


#-----------------------------------------------------------------------------#
# Objective functions
#-----------------------------------------------------------------------------#

def optLaunchAngle(angles, Vlaunch, simobj):
    """
    Objective funtion for launch angles (phi, theta) for a fixed initial 
    velocity and given sim object. Velocity is aligned with the initial launch
    attiudue.
    """
    
    ics = initconds(v0 = [Vlaunch, 0, 0], phi0 = angles[0], theta0 = angles[1],
                    gamma = 50.0)
    ics.setIc(simobj)
    
    simobj.integrate(20, 2000)
    
    return np.linalg.norm(simobj.x[-1, [0, 1]]), simobj

def optLaunchAngleAlpha(angles, Vlaunch, simobj):
    """
    Objective funtion for launch angles (phi, theta, deltaV) for a fixed 
    initial speed and given sim object. Initial velocity is adjusted to match
    the launch angle deltaV. 
    """
    
    v0 = [Vlaunch*np.cos(th/180*np.pi), 0.0, -Vlaunch*np.sin(th/180*np.pi)]
    ics = initconds(v0 = v0, phi0 = angles[0], theta0 = angles[1],
                    gamma = 50.0)
    ics.setIc(simobj, False)
    
    simobj.integrate(20, 2000)
    
    return np.linalg.norm(simobj.x[-1, [0, 1]]), simobj


#-----------------------------------------------------------------------------#
# Test cases
#-----------------------------------------------------------------------------#
# Locations for references:
# Crowther: https://www.researchgate.net/publication/268559957_FrisbeeTM_Aerodynamics
# Hummel: https://morleyfielddgc.files.wordpress.com/2009/04/hummelthesis.pdf
# 
# All page numbers refer to PDF pages, not paper pages

# Set up dictionaries based on verification case of interest
# Crowther: Figure 7.4, Page 197 for aerodynamics; Hummel Table 3-1 pg. 46
# for mass/inertia/radius. All units kg/m. I adjusted the aero derivatives
# on pitching moment to better fit the digitized wind tunnel data.
# Lissaman: Section 4.2, with the pitching moment from Crowther used. Defines
# drag coefficient in terms of the span efficiency e, convert to Cd below:
# Hummel: Table 3-1 page 46 for disc mass and size. 
e = 0.85
AR = 4/np.pi
cd_lissaman = 1/(np.pi*e*AR)
discdict = {'crowther': discprops(0.175, 0.27305*0.5, 0.00235, 0.00122,
                                  0.13, 3.09, 0.085, 3.30, -0.0143, 0.2107,
                                  -0.014, -0.00),
            'hummel': discprops(0.175, 0.269*0.5, 0.00235, 0.00122,
                                  0.2, 2.96, 0.08, 2.72, -0.02, 0.13,
                                  -0.014, -0.013),
            'lissaman': discprops(0.175, 0.27*0.5, 0.0025, 0.00125,
                                  0.15, 2.91, 0.033, cd_lissaman, -0.0143, 0.2107)}


# Initial conditions for verification cases of interest. 
# Crowther: Section 7.3.2 first paragraph; pg 95-96
# Hummel: Table 3-3, pg 54; P&C aerodynamic coefficients
# TODO: Add rate damping terms
alpha0 = 7.0
icdict = {'crowther': initconds(x0 = [0.0, 0.0, -1.0], 
                                v0 = [15.0, 0.0, 0.0],
                                theta0 = 15.0/180.0*np.pi,
                                gamma = 5*2*np.pi),
          'hummel': initconds(x0 = [-0.9, -0.63, -0.91], 
                              v0 = [13.42, -0.41, 0.001],
                              phi0 = -0.07,
                              theta0 = 0.21,
                              psi0 = 5.03,
                              thetad0 = -1.48,
                              phid0 = -14.94,
                              psid0 = 54.25,
                              gamma = None),
          'lissaman': initconds(v0 = [25.0*np.cos(alpha0/180*np.pi), 0.0, 
                                      25.0*np.sin(alpha0/180*np.pi)],
                                # v0 = [30.0, 0.0, 0.0],
                                phi0 = 0.0/180.0*np.pi,
                                theta0 = 15.0/180.0*np.pi,
                                gamma = 30.0)}



if __name__ == '__main__':
    
    # Other conditions
    rho = 1.225  # Atmospheric density
    g = 9.81  # Gravitational acceleration
    T = 10.0  # Max integration period (simulation terminates when disc lands)
    

    # Define some analytical relations for lift/drag/moment coefficients
    # "Short flights" values of Table 3-2 of Hummel; pg 50
    # Cl0 = 0.33; Clalpha = 1.91; Cd0 = 0.18; Cdalpha = 0.69; Cm0 = -0.08; Cmalpha = 0.043

    # Define testname
    # testname = 'lissaman'
    # testname = 'crowther'
    testname = 'hummel'

    # Set up problem
    testDisc = discdict[testname]
    m, r, Iaxis, Ioff, Cl0, Clalpha, Cd0, Cdalpha, Cm0, Cmalpha, Cmq, Crp \
                                                        = testDisc.getProps()

    # Note that Crowther gives a value of alpha0 which is not consistent with
    # the definition below. Not sure where the conflict lies; haven't double-
    # checked the data yet.
    alpha0 = -Cl0/Clalpha
    cl = lambda alpha: (Cl0 + Clalpha*alpha)
    if testname == 'lissaman':  # Lissaman uses span efficiency to define Cd
        cd = lambda alpha: Cd0 + cl(alpha)**2*Cdalpha
        cm = lambda x: 0.7158*x**3 + 0.0898*x**2 + 0.0942*x - 0.0097 # Lissaman
                                   
    elif testname == 'crowther':
        cd = lambda alpha: (Cd0 + Cdalpha*(alpha - alpha0)**2)
        cm = lambda x: 0.4785*x**3 + 0.0008*x**2 + 0.1041*x - 0.0119 # Crowther

    elif testname == 'hummel':
        cd = lambda alpha: (Cd0 + Cdalpha*(alpha - alpha0)**2)
        cm = lambda alpha: Cm0 + Cmalpha*alpha

    # Create test disc
    td = disc.disc(m, r)
    td.setInertia(Ioff, Ioff, Iaxis)
    td.setCl(cl)
    td.setCd(cd)
    td.setCm(cm)
    td.setRateDerivatives(Cmq, Crp)

    # Initial conditions
    ics = icdict[testname]
    
    # Create sim object; apply ICs
    ts = sim(td)
    ics.setIc(ts, False)
    ts.integrateEuler = True

    # Run simulation
    # The returned variables are all extra debug quantities. State variables
    # are stored in the sim object.
    Vb, alpha, sideslip, lift, drag, moment = ts.integrate(T, 1000)

    # Plot the disc motion
    nt = len(ts.t)
    
    Rq0 = transform.quat2r(ts.q0, ts.q40)
    Re0 = transform.euler2r(*ts.euler0)

    if testname == 'crowther':
        # Compare to Figure 7.3 of Crowther, Page 196
        plt.figure(figsize = (16, 4))
        ts.plotTrajectory(spacing = 10, view = 'top', color = 'k')
        plt.title('Figure 7.3 (a)')

        plt.figure(figsize = (16, 4))
        ts.plotTrajectory(spacing = 10, view = 'right')
        plt.title('Figure 7.3 (b)')
        
        plt.figure(figsize = (12, 4))
        speed = np.linalg.norm(ts.v, axis = 1)
        plt.plot(ts.t, speed)
        plt.title('Figure 7.3(d)')
        plt.xlabel('Time [s]')
        plt.ylabel('Speed [m/s]')

        plt.figure(figsize = (12, 4))
        plt.plot(ts.t, alpha/np.pi*180)
        plt.xlabel('Time [s]')
        plt.ylabel('Angle of Attack [deg]')
        plt.title('Figure 7.3(e)')

    if testname == 'lissaman':
        # Compare range to Table I, Free Case I of Lissaman
        plt.figure(figsize = (16, 4))
        ts.plotTrajectory(spacing = 10, view = 'top', color = 'k')
        plt.title('Figure 7.3 (a)')

        plt.figure(figsize = (16, 4))
        ts.plotTrajectory(spacing = 10, view = 'right')
        plt.title('Figure 7.3 (b)')
        
        plt.figure(figsize = (12, 4))
        speed = np.linalg.norm(ts.v, axis = 1)
        plt.plot(ts.t, speed)
        plt.title('Figure 7.3(d)')
        plt.xlabel('Time [s]')
        plt.ylabel('Speed [m/s]')

        plt.figure(figsize = (12, 4))
        plt.plot(ts.t, alpha/np.pi*180)
        plt.xlabel('Time [s]')
        plt.ylabel('Angle of Attack [deg]')
        plt.title('Figure 7.3(e)')
        
        plt.figure(figsize = (16, 4))
        distance = np.linalg.norm(ts.x[:, [0, 1]], axis = 1)
        plt.plot(distance, -ts.x[:, 2])
        plt.title('Final Distance = %.2f m' % distance[-1])
        
        
        plt.figure()
        td.plot(ts.y[:, 0], showPosition=False)
        
        plt.gca().view_init(0.0, 0.0)
        plt.xlabel('X')
        plt.ylabel('Y')
        plt.axis('equal')
        plt.gca().invert_zaxis()
        
    if testname == 'hummel':
        # Compare to Figure 3-9 of Hummel, page 56
        fig, ax = plt.subplots(2, 2)
        fig.set_figheight(12)
        fig.set_figwidth(12)
        ax = ax.flatten()
        
        # Position
        ax[0].plot(ts.t, ts.x[:, 0], 'k-', label = '$x$')
        ax[0].plot(ts.t, ts.x[:, 1], 'k--', label = '$y$')
        ax[0].plot(ts.t, ts.x[:, 2], 'k:', label = '$z$')
        ax[0].legend()
        ax[0].set_xlim([0, 1.5])
        ax[0].set_yticks([-3, 0, 3, 6, 9, 12, 15])
        ax[0].set_xticks([0.0, 0.5, 1.0, 1.5])
        ax[0].set_ylim([-3, 15])
        ax[0].set_xlabel('Time [s]')
        ax[0].set_ylabel('Position [m]')
        ax[0].grid('on')
        
        # Orientation
        ax[2].plot(ts.t, ts.euler[:, 0], 'k-', label = '$\phi$')
        ax[2].plot(ts.t, ts.euler[:, 1], 'k--', label = '$\theta$')
        
        # Velocity. Hummel must be plotting body-fixed velocity but our is 
        # contaminated by the yaw rate so we can't plot it. Once supplementary
        # Euler angles are back in the EOM we can use those to show the 
        # same body-fixed velocity as Hummel.
        v = np.zeros(ts.v.shape)
        for i in range(len(ts.t)):
        #for i in range(1):
            
            q = ts.q[i, :]
            q4 = ts.q4[i]
            Rbe = transform.quat2r(q, q4)
            
            e = ts.euler[i, :]
#            Rbe = transform.euler2r(e[0], e[1], e[2])
            v[i, :] = Rbe.T @ ts.v[i, :]
            
        ax[1].plot(ts.t, v[:, 0], 'k-', label = '$\dot{x}$')
        ax[1].plot(ts.t, v[:, 1], 'k--', label = '$\dot{y}$')
        ax[1].plot(ts.t, v[:, 2], 'k:', label = '$\dot{z}$')

        ax[1].legend()
        ax[1].set_xlim([0, 1.5])
        ax[1].set_ylim([-3, 15])
        ax[1].set_xlabel('Time [s]')
        ax[1].set_ylabel('Earth-Frame Velocity [m/s]')
        ax[1].set_yticks([-3, 0, 3, 6, 9, 12, 15])
        ax[1].set_xticks([0.0, 0.5, 1.0, 1.5])
        ax[1].grid('on')
        
        # Angular velocity
        ed = np.zeros((ts.euler.shape))
        for i in range(len(ts.t)):
            e = ts.euler[i, :]
            om = ts.omega[i, :]
            ed[i, :] = transform.omega2EulerRates(e, om)
            
#        ax[3].plot(ts.t, ed[:, 0])
#        ax[3].plot(ts.t, ed[:, 1])
#        ax[3].plot(ts.t, ed[:, 2]/10)
        ax[3].plot(ts.t, ts.omega[:, 0])
        ax[3].plot(ts.t, ts.omega[:, 1])
        ax[3].plot(ts.t, ts.omega[:, 2]/10)

        plt.figure()
        qmag = [np.sqrt(ts.q[i, 0]**2 + ts.q[i, 1]**2 + ts.q[i, 2]**2 + 
                        ts.q4[i]**2) for i in range(len(ts.t))]
        plt.plot(ts.t, qmag)

        
        
    