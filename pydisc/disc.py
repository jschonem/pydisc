# -*- coding: utf-8 -*-

import numpy as np
import transform
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

#-----------------------------------------------------------------------------#
# Main class definition
#-----------------------------------------------------------------------------#
class disc:
    """
    Base disc class. Contains methods to obtain key
    aerodynamic properties and derivatives. Also contains methods to visually plot
    the disc at a given configuration. Does NOT store state information, this is
    tracked and/or supplied externally by the sim class.
    """

    def __init__(self, mass, radius):
        """
        Initialize disc object and set key structural properties. Methods to
        obtain aerodynamic derivatives are defined externally and supplied to
        the disc object using "setC_" methods.
        """

        self.mass = mass
        self.radius = radius

        # Scalar properties
        self.c = 2.0*radius  # "Chord length" of the disc
        self.S = np.pi*radius*radius
        Iz = mass*radius*radius*0.5
        Ix = 0.5*Iz
        Iy = 0.5*Iz
        
        # Initialize the inertia properties to be set by functions below
        self.Ix = self.Iy = self.Iz = 0.0
        self.I = np.zeros((3, 3))
        self.Iinv = np.zeros((3, 3))
        self.setInertia(Ix, Iy, Iz)
        
        # Initialize the rate derivatives to default values
        self.Cmq = self.Crp = 0.0
        self.setRateDerivatives(Cmq = -0.01, Crp = -0.01)

    def setInertia(self, Ix, Iy, Iz):
        """
        Sets the inertia matrix to a non-default value
        """
        self.Ix = Ix
        self.Iy = Iy
        self.Iz = Iz

        # Matrix properties
        self.I = np.diag([self.Ix, self.Iy, self.Iz])
        self.Iinv = np.linalg.inv(self.I)
        
    def setRateDerivatives(self, Cmq = 0.0, Crp = 0.0):
        """
        Sets angular rate derivatives. Currently only the direct roll and pitch
        deratives are used. Note that these are defined to act along the wind
        axes rather than the body axes.
        """
        
        self.Cmq = Cmq
        self.Crp = Crp

    def getAeroForces(self, V, omega, rho):
        """
        Compute aerodynamic forces in the body-fixed frames, based on the
        body-fixed air velocity. Two additional transformations are used: A
        "zero-sideslip" yaw and a relative wind pitch. Coefficients are
        computed based on relative wind orientation and the "getC_" functions
        which may be implemented arbitrarily (e.g. linearized, lookup-table,
        etc.)

        See section 7.1 of Crowther for the process followed here.
        """

        # Compute velocity vector direction. This is the opposite of the drag
        # vector direction.
        vmag = np.linalg.norm(V)
        vhat = V/vmag

        # Compute the lift vector direction. First, find the vector normal to
        # the velocity and the disc normal direction.
        vnorm = np.cross(vhat, np.array([0.0, 0.0, -1.0]))
        vnorm /= np.linalg.norm(vnorm)
        
        # The lift vector is now normal to the drag direction (-vhat) and this
        # "vnorm" vector
        lhat = np.cross(-vhat, vnorm)
        lhat /= np.linalg.norm(lhat)

        # The "pitching moment" operates perpendiculuar to the velocity and
        # lift vectors
        mhat = np.cross(vhat, lhat)
        mhat /= np.linalg.norm(mhat)

        # The vehicle "up" vector is [0, 0, -1]. This can be used to find
        # the angle of attack relative to the vehicle plane whose normal is
        # the up vector.
        alpha = np.arcsin(vhat[-1])

        # Get aerodynamic derivatives
        # Note: Not convinced about the "rotational transformation matrix"
        # in Eq. 7.2 of Crowther. That's a differential equation, not a
        # matrix transformation. I could be wrong. In any case, we don't
        # have data for the angular rate derivatives so they're not currently
        # implemented.
        Cd = self.getCd(alpha)
        Cl = self.getCl(alpha)
        Cm = self.getCm(alpha)

        # Convert to dimensional values
        q = 0.5*rho*vmag*vmag
        drag = -q*self.S*Cd*vhat
        lift = q*self.S*Cl*lhat
    
        # Set the pitching moment. Rate terms are evaluated about the wind
        # axes, not the body axes, as per Crowther. I think this makes the most
        # sense, even though the concept of a rate derivative probably isn't
        # fully justified for a spinning disc.
        pitchMoment = q*self.S*self.c*(Cm + self.Cmq*np.dot(omega, mhat))*mhat
        pitchMoment += q*self.S*self.c*(self.Crp*np.dot(omega, vhat))*vhat
#                           
        # This alternate representation applies forces about the body axes
        # rather than the wind-based coordinate system            
#        pitchMoment = q*self.S*self.c*np.array([self.Crp*omega[0], self.Cmq*omega[1], 0.0])

        return lift + drag, pitchMoment

    def getAngularMomentum(self, omega):
        """
        Obtain the current angular momentum of the disc based on rotation
        rates omega and spin rate gamma (added to the Z axis)
        """

        return self.I @ omega

    def getCl(self, alpha):
        """
        Placeholder function for getCl. This is intended to be replaced by
        an argument to setCl.
        """
        raise NotImplementedError

    def getCd(self, alpha):
        """
        Placeholder function for getCl. This is intended to be replaced by
        an argument to setCl.
        """
        raise NotImplementedError

    def getCm(self, alpha):
        """
        Placeholder function for getCl. This is intended to be replaced by
        an argument to setCl.
        """
        raise NotImplementedError

    def getInertiaInv(self):
        """
        Returns inverse of the inertia matrix.
        """
        return self.Iinv

    def getMass(self):
        """
        Return mass of disc.
        """
        return self.mass

    def plot(self, x, showPosition = True, **kwargs):
        """
        Plots the disc based on the supplied vector of state variables. The
        "showPosition" argument will plot the disc at the current position if
        set to True, or centered at the origin if set to False.
        """

        # Obtain variables of interest
        if showPosition:
            Xe = x[:3]  # Earth-fixed position
        else:
            Xe = np.zeros((3, ), dtype = float)
        q = x[9:12]
        q4 = x[12]
        R = transform.quat2r(q, q4).T

        ax = plt.gca(projection = '3d')
        theta = np.linspace(0, 2*np.pi)
        X = R @ np.vstack([self.radius*np.cos(theta),
                       self.radius*np.sin(theta),
                        np.zeros((len(theta), ))])


        ax.plot(Xe[0] + X[0, :], Xe[1] + X[1, :], Xe[2] + X[2, :], **kwargs)
        
    def setCl(self, clfun):
        """
        Assigns the callable clfun(alpha) to self.getCl. clfun should accept
        a float angle of attack (radians) and return a float coefficient of
        lift.
        """
        assert isinstance(clfun(0.0), float)
        self.getCl = clfun

    def setCd(self, cdfun):
        """
        Assigns the callable cdfun(alpha) to self.getCd. cdfun should accept
        a float angle of attack (radians) and return a float coefficient of
        drag.
        """
        assert isinstance(cdfun(0.0), float)
        self.getCd = cdfun

    def setCm(self, cmfun):
        """
        Assigns the callable cmfun(alpha) to self.getCm. cmfun should accept
        a float angle of attack (radians) and return a float coefficient of
        pitching moment.
        """
        assert isinstance(cmfun(0.0), float)
        self.getCm = cmfun

#-----------------------------------------------------------------------------#
# Test cases
#-----------------------------------------------------------------------------#

if __name__ == '__main__':

    # For each disc, the mass and radius were set to 0.177 kg and 0.107 m,
    # (Page 85 of Dyn of Flying Discs)
    m = 1.77  # Mass [kg]
    r = 0.107  # Radius [m]

    # Define some analytical relations for lift/drag/moment coefficients
    # Figure 7.4 of Crowther; page 197
    cl = lambda alpha: 0.13 + 3.09*alpha
    cd = lambda alpha: 0.085 + 3.30*(alpha + 0.052)**2
    cm = lambda alpha: -0.01 + 0.057*alpha  # We can do better with a cubic

    # Create test disc
    td = disc(m, r)
    td.setCl(cl)
    td.setCd(cd)
    td.setCm(cm)

    # Test out the aerodynamic force/moment generation
    rho = 1.225
    alpha = 0.1
    V = 20.0*np.array([np.cos(alpha), 0.0, np.sin(alpha)])
    omega = np.zeros((3, ))
    force, moment = td.getAeroForces(V, omega, rho)

    # Test out the angular momentum generation
    omega = np.array([1.0, 0.01, 0.1])
    gamma = 5.0
    h = td.getAngularMomentum(omega, gamma)

    # Test plot routine
    axis = np.array([1.0, 0.0, 0.0])
    angle = -75/180*np.pi
    qq, qq4 = transform.angleAxis2quat(angle, axis)
    R = transform.quat2r(qq, qq4)
    Re = transform.euler2r(angle, 0.0, 0.0)
    q, q4 = transform.r2quat(Re)
    
    x = np.zeros((14, ))
    x[9:12] = q
    x[12] = q4

    td.plot(x)
    plt.gca().view_init(0, 0)
    plt.xlabel('x')
    plt.ylabel('y')
    plt.axis('equal')
    plt.gca().invert_zaxis()
    