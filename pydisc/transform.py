# -*- coding: utf-8 -*-
"""
Stores coordinate transformation functions.
"""

import numpy as np

#-----------------------------------------------------------------------------#
# Transformations and conversions
#-----------------------------------------------------------------------------#

def euler2r(phi, theta, psi):
    """
    Compute the rotation matrix corresponding to body-fixed rotations in 3-2-1
    sequence defined by Etkin in 44 (p. 94). This returns the matrix Leb which
    is defined as Eq. 4.4,2 or 4.4,3. Angles must be floats.
    
    Note that the multiplication order of the matrices is opposite that of
    the rotations.
    """
    
    assert (isinstance(phi, float) and isinstance(theta, float) 
            and isinstance(psi, float))
    
    r3 = rotmatrix(-psi, 3)
    r2 = rotmatrix(-theta, 2)
    r1 = rotmatrix(-phi, 1)
    
    return (r3 @ r2 @ r1)

def eulerRates2Omega(phi, theta, psi, phid, thetad, psid):
    """
    Converts initial Euler rates to body-fixed angular velocities. Only defined
    for the 3-2-1 ordering; Eq. 4.4,6 of Etkin.
    """
 
    rates = np.array([phid, thetad, psid])
    stheta = np.sin(theta)
    sphi = np.sin(phi)
    ctheta = np.cos(theta)
    cphi = np.cos(phi)
    R = np.array([[1.0, 0.0, -stheta], 
                  [0.0, cphi, sphi*ctheta],
                  [0.0, -sphi, cphi*ctheta]])
    return R @ rates   

def omega2EulerRates(eulerAngles, omegab):
    """
    Converts body-fixed angular rates to the Euler angle rates of 
    change at a given orientation phi, theta, psi. Only defined for the 3-2-1
    given by Etkin per Eq. 4.4,7, where eulerAngles is defined as [phi, theta,
    psi]
    """
    
    phi, theta, psi = eulerAngles
    
    sphi = np.sin(phi)
    stheta = np.sin(theta)
    
    cphi = np.cos(phi)
    ctheta = np.cos(theta)
    
    ttheta = stheta/ctheta
    sectheta = 1.0/ctheta
    
    T = np.array([[1.0, sphi*ttheta, cphi*ttheta], 
                  [0.0, cphi, -sphi],
                  [0.0, sphi*sectheta, cphi*sectheta]])
    
    return T @ omegab   
    
    
    

def quat2r(q3, q4):
    """
    Convert the quaternion defined by length-3, dim-1 q3 and float q4 to its
    corresponding rotation matrix R.
    """
    
    assert (isinstance(q3, np.ndarray))
    assert (len(q3) == 3)
    assert (q3.ndim == 1)
    assert (isinstance(q4, float))
    
    q3 = q3.reshape((3, 1))
    I3 = np.eye(3, dtype = float)
    Q = skew(q3)
    
    return ((q4*q4 - q3.T @ q3) * I3 + 2.0*q3 @ q3.T - 2.0*q4*Q)

def rotmatrix(theta, axis):
    """
    Returns a matrix corresponding to the rotation theta (radians) about the 
    axis "axis" (1, 2 or 3)
    """
    
    assert isinstance(theta, float)
    assert isinstance(axis, int)
    assert (axis == 1 or axis == 2 or axis == 3)
    
    inds = np.array([1, 2, 3], dtype = int)
    inds = inds != axis
    if (axis == 1) or (axis == 3):
        sign = 1.0
    else:
        sign = -1.0
    R = np.eye(3, dtype = float)
    R[np.ix_(inds, inds)] = np.array([[np.cos(theta), -sign*np.sin(theta)],
                                      [sign*np.sin(theta), np.cos(theta)]])
    return R
    
def r2quat(R):
    """
    Convert the 3 x 3 rotation matrix R to a quaternion given by length-3, 
    dim1 q3 and float q4.
    """
    
    assert (isinstance(R, np.ndarray))
    assert (R.ndim == 2)
    assert (R.shape[0] == 3)
    assert (R.shape[1] == 3)
    
    q4 = 0.5*np.sqrt(1 + R[0, 0] + R[1, 1] + R[2, 2])
    q3 = 0.25/q4*np.array([R[1, 2] - R[2, 1], 
                           R[2, 0] - R[0, 2], 
                           R[0, 1] - R[1, 0]])
    
    return q3, q4

def angleAxis2quat(angle, axis):
    """
    Define a quaternion (q3, q4) from an angular rotation (in radians) and 
    an axis of rotation. The axis is normalized before use.
    """
    
    assert (isinstance(angle, float))
    assert (type(axis) is np.ndarray)
    assert (len(axis) == 3)
    
    
    axis *= (np.sin(0.5*angle))/np.linalg.norm(axis)
    q4 = np.cos(0.5*angle)
    
    return axis, q4
    
def skew(w):
    """
    Convert the length-3 vector w to the 3x3 skew-symmetric matrix equivalent.
    """
    
    assert (len(w) == 3)
    
    return np.array([[0.0, -w[2], w[1]],
                     [w[2],  0.0, -w[0]],
                     [-w[1], w[0], 0.0]], dtype = float)
             
#-----------------------------------------------------------------------------#
# Kinematic equations
#-----------------------------------------------------------------------------#

def qdot(q, q4, omega):
    """
    Obtain the time rate of change of a quaternion based on current orientation 
    (q, q4) and body-fixed angular rates omega.
    """
    
    assert (isinstance(q, np.ndarray) and isinstance(omega, np.ndarray))
    assert (len(q) == 3 and len(omega) == 3)
    assert (q.ndim == 1 and omega.ndim == 1)
    assert isinstance(q4, float)
    
    qd = 0.5*(q4*omega - skew(omega) @ q)
    q4d = -0.5*omega.T @ q
    
    return qd, q4d

#-----------------------------------------------------------------------------#
# Test cases
#-----------------------------------------------------------------------------#

if __name__ == '__main__':
    # In this case run test functions
    
    # Check the Euler angle conversion functions
    e0 = np.array([-0.07, 0.21, 5.03])
    ed0 = np.array([-14.94, -1.48, 54.25])
    
    Reuler = euler2r(*e0)
    q0, q40 = r2quat(Reuler)
    Rq = quat2r(q0, q40)
    
    omd0 = eulerRates2Omega(e0[0], e0[1], e0[2], ed0[0], ed0[1], ed0[2])
    ed0Check = omega2EulerRates(e0, omd0)
    
    phi, theta, psi = e0
    sphi = np.sin(phi)
    stheta = np.sin(theta)
    
    cphi = np.cos(phi)
    ctheta = np.cos(theta)
    
    ttheta = stheta/ctheta
    sectheta = 1.0/ctheta
    
    R = np.array([[1.0, 0.0, -stheta], 
                  [0.0, cphi, sphi*ctheta],
                  [0.0, -sphi, cphi*ctheta]])
        
    T = np.array([[1.0, sphi*ttheta, cphi*ttheta], 
                  [0.0, cphi, -sphi],
                  [0.0, sphi*sectheta, cphi*sectheta]])
    
    
    
    