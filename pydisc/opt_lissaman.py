# -*- coding: utf-8 -*-
"""

Optimizes launch angles for Lissaman test case disc.

"""

import disc
import sim
import numpy as np
import scipy.optimize 


# Set up disc and sim obect
testDisc = sim.discdict['lissaman']
m, r, Iaxis, Ioff, Cl0, Clalpha, Cd0, Cdalpha, Cm0, Cmalpha = testDisc.getProps()
rho = 1.225  # Atmospheric density
g = 9.81  # Gravitational acceleration
    
# Note that Crowther gives a value of alpha0 which is not consistent with
# the definition below. Not sure where the conflict lies; haven't double-
# checked the data yet.
alpha0 = -Cl0/Clalpha
cl = lambda alpha: (Cl0 + Clalpha*alpha)
cd = lambda alpha: Cd0 + cl(alpha)**2*Cdalpha
cm = lambda x: 0.7158*x**3 + 0.0898*x**2 + 0.0942*x - 0.0097 # Lissaman


# Create test disc
td = disc.disc(m, r)
td.setCl(cl)
td.setCd(cd)
td.setCm(cm)
td.setInertia(Ioff, Ioff, Iaxis)

# Create test sim object
simobj = sim.sim(td)

# Set launch velocity
Vlaunch = 30.0

# Create objective unction
fun = lambda angles: -sim.optLaunchAngle(angles, Vlaunch, simobj)[0]
angle0 = np.array([0.0, 0.2])
optAngles = scipy.optimize.fmin(fun, angle0)

refDist, refSim = sim.optLaunchAngle(angle0, Vlaunch, simobj)
optDist, optSim = sim.optLaunchAngle(optAngles, Vlaunch, simobj)

fun3 = lambda angles: -sim.optLaunchAngleAlpha(angles, Vlaunch, simobj)[0]
angle0 = np.array([-71/180*np.pi, 0.0, 41/180*np.pi])
optAngles3 = scipy.optimize.fmin(fun3, angle0)

refDist3, refTraj3 = sim.optLaunchAngle(angle0, Vlaunch, simobj)
optDist3, optTraj = sim.optLaunchAngleAlpha(optAngles3, Vlaunch, simobj)


